﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallax_boss : MonoBehaviour {

	// <-------------> VARIABLES PUBLIC
	public parallax_plan[] plans;
	public Camera the_camera;
	// <-------------> VARIABLES PRIVATE
	private Vector2 start_position, current_position;
	private float new_offset, current_offset;
	
	// PRIMARY FUNCTION MONO
	void Awake() {
		start_position = the_camera.transform.position;
	}
	void Update() {
		UpdateOffset();
	}
	
	// PRIMARY FUNCTIONS
	private void UpdateOffset() { // Every frame look at the camera offset.
		current_position = the_camera.transform.position;
		new_offset = (current_position - start_position).x;
		if (new_offset == current_offset) return;
		current_offset = new_offset;
		ApplyOffsetToPlans(current_offset);
	}
	private void ApplyOffsetToPlans(float offset) { // Communicate the new offset to plans.
		foreach(parallax_plan p in plans) {
			p.Move(offset);
		}
	}
}