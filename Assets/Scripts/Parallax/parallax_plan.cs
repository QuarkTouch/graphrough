﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallax_plan : MonoBehaviour
{
    public float speed_factor;
    private Vector2 start_position;
    
    // PRIMARY FINUNCTION MONO
    void Awake() {
    	start_position = transform.position;
    }
    
    // GLOBAL FUNCTIONS
    public void Move(float offset) { // Move the plan according to the camera offset.
    	transform.position = start_position + new Vector2(-offset*speed_factor,0);
    }
}
